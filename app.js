function processData(data) {
  let result = 0;

  // Bloque de lógica con demasiada complejidad
  for (let i = 0; i < data.length; i++) {
    if (data[i] > 0) {
      result += data[i];
    } else if (data[i] < 0) {
      result -= data[i];
    } else {
      result = 0;
    }
  }

  return result;
}