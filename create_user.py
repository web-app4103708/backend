class UserManager:
    def create_user(self, name, email):
        print(f"Creating user: {name} with email {email}")

def some_function():
    name = "John Doe"
    email = "john.doe@example.com"
    # Aquí se pasa 'self' incorrectamente como argumento
    UserManager.create_user(self, name, email)  # Error: Unexpected keyword argument 'self'

# Error de sintaxis: falta paréntesis en la definición de la función
def my_function:
    print("Hello")

# Error de variable no definida
print(x)

def my_function():
print("Hello")  # Indentación incorrecta

# Intentando sumar un int y un string
result = 1 + "2"  # Error de tipo

def my_function():
    x = 5
    y = 10
    # 'y' no se usa
    print(x)

def my_function():
    return 42  # Falta docstring

class MyClass:
    def my_method(self):
        print("Hello")

obj = MyClass()
obj.my_method(self=obj)  # Incorrecto, 'self' debe ser gestionado por el método de la clase


print("Debugging")  # Usar logging en lugar de print

def my_function(a, b, c, d, e, f, g, h, i):
    pass  # Demasiados parámetros